/* https://www.reddit.com/r/dailyprogrammer/comments/2yquvm/20150311_challenge_205_intermediate_rpn/
 * challenge part, as separate program, evaluate the rpn expressions
 * run: ./torpn < rpnin | ./rpneval
 * lex() is exactly the same as torpn.c except for removal of ()
 */
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

/* operators represent themselves */
enum {
    NUM = CHAR_MAX + 1,
    ERR,
};
int val;

int lex(void) {
    int c;

    while (isblank(c = getchar())) /* ignore whitespace */
        ;
    if (c == EOF)
        return EOF;
    if (isdigit(c)) { /* got a number */
        val = 0;
        do val = val * 10 + c - '0';
        while (isdigit(c = getchar()));
        ungetc(c, stdin);
        return NUM;
    }
    if (!strchr("+-/x*\n", c)) {/* bad token */
        fprintf(stderr, "bad character: '%c'(%d)\n", c, c);
        return ERR;
    }
    return c;
}

int eval(void)
{
    int tok, stack[100], *top = stack; /* FIXME: fixed size stack */

    while ((tok = lex()) != EOF && tok != '\n' && tok != ERR) {
        if (tok != NUM && top - stack < 2) {
            fprintf(stderr, "not enough numbers for operator: %c\n", tok);
            return ERR;
        }
        switch (tok) {
        case NUM: *top++ = val; break;
        case 'x': /* fall through */
        case '*': top[-2] = top[-2] * top[-1]; top--; break;
        case '/': top[-2] = top[-2] / top[-1]; top--; break;
        case '+': top[-2] = top[-2] + top[-1]; top--; break;
        case '-': top[-2] = top[-2] - top[-1]; top--; break;
        }
    }
    if (tok == EOF)
        return 0;
    if (--top != stack) {
        fprintf(stderr, "not enough operators\n");
        return ERR;
    }
    printf("%d\n", *stack);
    return 1;
}

int main(void)
{
    while (eval())
        ;
    return 0;
}

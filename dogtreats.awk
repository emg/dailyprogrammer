# https://www.reddit.com/r/dailyprogrammer/related/3629st/20150515_challenge_214_hard_chester_the_greedy/
NR != 1 { x[NR] = $1; y[NR] = $2 }
END {
    cx = cy = 0.5
    while (--NR) {
        min = 2
        for (k in x) {
            dist = sqrt((cx - x[k])^2 + (cy - y[k])^2)
            if (dist < min) {
                min = dist
                nk = k
                nx = x[k]
                ny = y[k]
            }
        }
        delete x[nk]
        delete y[nk]
        cx = nx
        cy = ny
        sum += min
    }
    print sum
}

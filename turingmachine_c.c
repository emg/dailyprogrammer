/* https://www.reddit.com/r/dailyprogrammer/comments/31aja8/20150403_challenge_208_hard_the_universal_machine/
 * assume valid input
 * don't free anything, let the OS handle that
 * erealloc and estrdup don't make the code shorter, but they do make it easier to read
 * the machine is simple, parsing the input is painful, what can I do better?
 */
#define _POSIX_C_SOURCE 200809L

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* could remove ins and do table[instate][insym]... */
typedef struct {
    char instate, insym, outstate, outsym, headdir;
} Transition;

typedef struct {
    Transition *table;
    size_t      size; /* of table */
    char        state, initstate, acceptstate;
    char       *tape, *tapeend, *head, *center;
} Program;

void *erealloc(void *p, size_t n)
{
    if (!(p = realloc(p, n)))
        exit(errno);
    return p;
}

char *estrdup(void *s)
{
    if (!(s = strdup(s)))
        exit(errno);
    return s;
}

ssize_t egetline(char **line, size_t *len, FILE *stream)
{
    ssize_t n = getline(line, len, stream);
    if (n < 0 && feof(stream))
        return EOF;
    else if (n < 0)
        exit(errno);
    if ((*line)[n - 1] == '\n')
        (*line)[--n] = '\0';
    return n;
}

char findstate(char **states, char *s)
{
    for (char **p = states; *p; p++)
        if (!strcmp(*p, s))
            return p - states;
    exit(2); /* not reached assuming valid input */
}

void input(Program *prog)
{
    char **states = NULL, *line = NULL, *p;
    size_t len = 0, n;

    /* ignore alphabet */
    egetline(&line, &len, stdin);

    /* get space separated list of states, lazy O(n) reallocation */
    egetline(&line, &len, stdin);
    for (n = 0, p = strtok(line, " \n"); p; n++, p = strtok(NULL, " \n")) {
        states        = erealloc(states, sizeof(*states) * (n + 2)); /* room for NULL */
        states[n]     = estrdup(p);
        states[n + 1] = NULL;
    }

    /* get start state */
    egetline(&line, &len, stdin);
    prog->state = prog->initstate = findstate(states, line);

    /* get accept state */
    egetline(&line, &len, stdin);
    prog->acceptstate = findstate(states, line);

    /* get initial tape */
    n = egetline(&line, &len, stdin);
    prog->center  = prog->head = prog->tape = estrdup(line);
    prog->tapeend = prog->tape + n;

    /* read transitions, lazy O(n) reallocation */
    prog->table = NULL;
    prog->size  = 0;
    while (egetline(&line, &len, stdin) != EOF) {
        Transition *t;

        prog->table = erealloc(prog->table, sizeof(*prog->table) * (prog->size + 2));
        t = prog->table + prog->size++;
        t[1].headdir = 0; /* used as NULL terminator */

        t->instate  = findstate(states, strtok(line, " "));
        t->insym    = *strtok(NULL, " ");
        strtok(NULL, " "); /* ignore the '=' */
        t->outstate = findstate(states, strtok(NULL, " "));
        t->outsym   = *strtok(NULL, " ");
        t->headdir  = *strtok(NULL, " ");
    }
}

void print(Program *prog)
{
    char *beg, *end;
    /* find first and last things we need to print, either initialized cell, center, or head */
    for (beg = prog->tape; *beg == '_' && beg != prog->head && beg != prog->center; beg++)
        ;
    for (end = prog->tapeend - 1; *end == '_' && end != prog->head && end != prog->center; end--)
        ;
    for (char *p = beg; p <= end; p++)
        putchar(*p);
    putchar('\n');
    for (char *p = beg; p <= end; p++)
        if      (p == prog->center) putchar('|');
        else if (p == prog->head  ) putchar('^');
        else                        putchar(' ');
    putchar('\n');
}

/* double tape size in positive or negative direction depending on position of head */
void grow(Program *prog)
{
    ptrdiff_t center  = prog->center  - prog->tape;
    ptrdiff_t head    = prog->head    - prog->tape;
    size_t    tapecap = prog->tapeend - prog->tape;

    prog->tape    = erealloc(prog->tape, sizeof(*prog->tape) * tapecap * 2);
    prog->center  = prog->tape + center;
    prog->head    = prog->tape + head;
    prog->tapeend = prog->tape + tapecap * 2;

    if (head < 0) { /* could have pos and neg tape instead of memcpy()ing everything around */
        memcpy(prog->tape + tapecap, prog->tape, sizeof(*prog->tape) * tapecap);
        memset(prog->tape, '_', sizeof(*prog->tape) * tapecap);
        prog->center  += tapecap;
        prog->head    += tapecap;
    } else {
        memset(prog->tape + tapecap, '_', sizeof(*prog->tape) * tapecap);
    }
}

void run(Program *prog)
{
    while (prog->state != prog->acceptstate) {
        Transition *t;
        for (t = prog->table; t->headdir; t++)
            if (t->instate == prog->state && t->insym == *prog->head)
                break;
        if (!t->headdir)
            exit(3); /* not reached assuming valid input */

        prog->state = t->outstate;
        *prog->head = t->outsym;
        prog->head += (t->headdir == '>') - (t->headdir == '<');
        if (prog->head < prog->tape || prog->head == prog->tapeend)
            grow(prog);
    }
}

int main(void)
{
    Program prog;
    input(&prog);
    run  (&prog);
    print(&prog);
    return 0;
}

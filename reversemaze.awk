# https://www.reddit.com/r/dailyprogrammer/comments/34izkl/20150501_challenge_212_hard_reverse_maze/
# assumes correct input
# to check output assuming bash:
# diff <(sort solution) <(awk -f reversemaze.awk input | sort -u)
# NOTE: example had a 2 digit number, non of the inputs did, handle it just to be safe
NR == 1 {
    h = $0
    next
}
NR == 2 {
    w = length
}
NR <= h + 1 { 
    for (i = 1; i <= length; i++)
        maze[NR - 1,i] = substr($0, i, 1) != " "
    next
}
{
    while (match($0, /^([rl]|[[:digit:]]+)/)) {
        moves[++nmoves] = substr($0, 1, RLENGTH)
        $0 = substr($0, RLENGTH + 1)
    }
}
END {
    for (si = 1; si <= h; si++) { # si, sj, sd: starting i, j, direction
        for (sj = 1; sj <= w; sj++) {
            if (maze[si,sj])
                continue
            for (sd = 0; sd < 4; sd++) { # 0 1 2 3 -> N E S W -> -i +j +i -j
                i = si; j = sj; d = sd; fail = 0
                for (m = 1; m <= nmoves && !fail; m++) {
                    if      (moves[m] == "r") d = (d + 1) % 4
                    else if (moves[m] == "l") d = (d + 3) % 4
                    else for (n = 0; n < moves[m]; n++) {
                        i += (d == 2) - (d == 0)
                        j += (d == 1) - (d == 3)
                        if (maze[i,j]) {
                            fail = 1
                            break
                        }
                    }
                }
                if (!fail)
                    printf("From (%d, %d) to (%d, %d)\n", sj - 1, si - 1, j - 1, i - 1)
            }
        }
    }
}

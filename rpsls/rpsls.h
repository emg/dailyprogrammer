enum {
    ROCK,
    PAPER,
    SCISSORS,
    LIZARD,
    SPOCK,

    NHANDS
};

char *names[] = {
    [ROCK    ] = "Rock",
    [PAPER   ] = "Paper",
    [SCISSORS] = "Scissors",
    [LIZARD  ] = "Lizard",
    [SPOCK   ] = "Spock",
};

#define BIT(a) (1 << (a))
#define WIN(a, b) !!(win[(a)] & BIT(b))
int win[] = {
    [ROCK    ] = BIT(SCISSORS) | BIT(LIZARD  ),
    [PAPER   ] = BIT(ROCK    ) | BIT(SPOCK   ),
    [SCISSORS] = BIT(PAPER   ) | BIT(LIZARD  ),
    [LIZARD  ] = BIT(PAPER   ) | BIT(SPOCK   ),
    [SPOCK   ] = BIT(ROCK    ) | BIT(SCISSORS),
};
int lose[] = {
    [ROCK    ] = BIT(PAPER   ) | BIT(SPOCK   ),
    [PAPER   ] = BIT(SCISSORS) | BIT(LIZARD  ),
    [SCISSORS] = BIT(ROCK    ) | BIT(SPOCK   ),
    [LIZARD  ] = BIT(ROCK    ) | BIT(SCISSORS),
    [SPOCK   ] = BIT(PAPER   ) | BIT(LIZARD  ),
};

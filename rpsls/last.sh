#!/bin/sh
#
# Always play a hand that would beat the last hand the opponent played

echo Rock
while read hand; do
    case $hand in
        Rock)     echo Paper    ;;
        Paper)    echo Scissors ;;
        Scissors) echo Spock    ;;
        Spock)    echo Lizard   ;;
        Lizard)   echo Rock     ;;
    esac
done

/* intermediate bot described at
 * https://www.reddit.com/r/dailyprogrammer/comments/23qy19/4232014_challenge_159_intermediate_rock_paper/
 * assumes valid input
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <search.h>

#include "rpsls.h"

int scmp(const void *p1, const void *p2)
{
    return strcmp(*(char **)p1, *(char **)p2);
}

int main(void)
{
    int nthrows[NHANDS] = { 0 }, max = 0;

    if (setvbuf(stdout, NULL, _IOLBF, BUFSIZ))
        return 1;

    for (;;) {
        char **hand, buf[] = "Scissors\n", *p = buf; /* longest hand */
        int counters = 0, ncounters = 0;
        size_t len;

        /* find most thrown hands, find counter throws */
        for (int i = 0; i < NHANDS; i++)
            if (nthrows[i] == max)
                counters |= lose[i];

        /* if any of the counters is a most thrown hand, remove it
         * keep track of how many counters we have */
        for (int i = 0; i < NHANDS; i++) {
            if (nthrows[i] == max && counters & BIT(i))
                counters &= ~BIT(i);
            if (counters & BIT(i))
                ncounters++;
        }

        if (!ncounters)
            puts(names[rand() % NHANDS]);
        else
            for (int i = 0, j = rand() % ncounters; i < NHANDS && j >= 0; i++)
                if (counters & BIT(i) && !j--)
                    if (puts(names[i]) == EOF)
                        return 1;

        if (!fgets(buf, sizeof(buf), stdin))
            return 1;

        buf[strlen(buf) - 1] = '\0';

        len = sizeof(names) / sizeof(*names);
        if (!(hand = lfind(&p, names, &len, sizeof(*names), scmp)))
            return 1;

        if (++nthrows[hand - names] > max)
            max = nthrows[hand - names];
    }
    return 0; /* not reached */
}

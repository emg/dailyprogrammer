# NOTE: fflush is not specified by POSIX! use gawk/nawk/mawk/etc.
function hand() {
    print hands[int(rand() * nhands) + 1]
    fflush()
}
BEGIN {
    nhands = split("Rock Paper Scissors Lizard Spock", hands)
    hand()
}
{
    hand()
}

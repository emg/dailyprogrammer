Rock Paper Scissors Lizard Spock
================================
An engine and example bots to face off in the game of "Rock Paper
Scissors Lizard Spock," inspired by the /r/dailyprogrammer series of
problems [0][1][2]


Bots
----
Bots must continuously write a single line to stdout with one of the
hands, then read a single line from stdin which is the hand the opponent
threw. Make sure to flush output or set stdout to line buffered, otherwise
the output will be buffered and never make it to the engine. The input
to your bot will be, and the output from your bot must be, exactly one
of the following lines:

Rock
Paper
Scissors
Lizard
Spock

engine.c
--------
Runs two bots against eachother keeping track of the score. e.g.

    ./engine random spock 1000

will run the random and spock bots against eachother 1000 times and
print the resulting score.

Example Bots
------------
random.awk: Throw a random hand each time. Note the need for fflush
   necessitates gawk/nawk/mawk/etc. as POSIX does not require fflush.

last.sh: Throw a hand that would beat the last hand every time.

spock.c: Throw Spock every time.

intermediate.c: Implement intermediate bot from [1].


[0] https://www.reddit.com/r/dailyprogrammer/comments/23lfrf/4212014_challenge_159_easy_rock_paper_scissors/
[1] https://www.reddit.com/r/dailyprogrammer/comments/23qy19/4232014_challenge_159_intermediate_rock_paper/
[2] https://www.reddit.com/r/dailyprogrammer/comments/23yinj/4252014_challenge_159_hard_rock_paper_scissors/

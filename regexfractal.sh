#!/bin/sh
# https://www.reddit.com/r/dailyprogrammer/comments/2fkh8u/9052014_challenge_178_hard_regular_expression/
# output is an ascii .ppm file, I use display from imagemagick to display it
# ./regexfractal 1024 '(13|31|24|42)' > frac.ppm && display frac.ppm
# examples: https://imgur.com/a/81Ox4
#
# started out as awk, but when the pictures got big the memory grew too much,
# so I spit out pixels as they are calculated and let sort take care of order
# POSIX EREs don't support backreferences
# POSIX awk doesn't provide access to capture groups
# instead, use length of match, .*match, match.* for red, green, and blue
# respectively (differentiate between no match and zero length match)
# tested with gawk, nawk, mawk, busybox awk, 9base awk
# nawk is fast, use it
# mawk is giving incorrect results, not sure why yet

# yay error checking! it doubles the size of programs! stupid users.
ispow2() { [ "$1" -ne 0 ] && [ "$(($1 & $1 - 1))" -eq 0 ]; }
isnum() { case $1 in *[![:digit:]]*) return 1;; esac; return 0; }

if [ "$#" -ne 2 ] || ! [ "$1" ] || ! isnum "$1" || ! ispow2 "$1"; then
    cat >&2 << EOF
USAGE: $0 size regex

size : must be a power of 2
regex: is a POSIX ERE

output is an ascii .ppm file
EOF
    exit 1
fi

awk -v "size=$1" -v "regex=$2" '
function mlen(n, regex) {
    match(n, regex)
    if (RLENGTH + 1 > max)
        max = RLENGTH + 1
    return RLENGTH + 1
}
function check(subsize, n, i, j,    q) {
    if (subsize == 1)
        printf("%d %d %d %d %d\n", i, j,
               mlen(n, regex), mlen(n, ".*"regex), mlen(n, regex".*"))
    else for (q = 1; q <= 4; q++)
        check(subsize / 2, n q,
              i + subsize / 2 * (q == 3 || q == 4),
              j + subsize / 2 * (q == 1 || q == 4))
}
BEGIN { 
    check(size, "", 0, 0)
    printf("-1 -1 P3 %d %d %d\n", size, size, max)
}
' | sort -k 1n -k 2n | cut -d ' ' -f 3-

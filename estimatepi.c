/* https://www.reddit.com/r/dailyprogrammer/comments/3f0hzk/20150729_challenge_225_intermediate_estimating_pi/
 * this program expects and ascii rgb ppm, use convert from ImageMagick:
 * convert -compress none circles.png ppm:- | ./estimatepi
 */
#include <stdio.h>
#include <stdlib.h>

typedef struct Circle Circle;
struct Circle {
    Circle *next; /* linked list of circles                            */
    int y;        /* y coordinate we last saw this circle on           */
    int x1, x2;   /* x coord of first and one past last pixel on row y */
    int a, d;     /* area, diameter                                    */
};

void update(Circle **list, int beg, int end, int y)
{
    Circle *cp;

    for (cp = *list; cp; cp = cp->next)
        if (y == cp->y + 1 &&
            ((beg <= cp->x1 && end >= cp->x2) ||
             (beg >= cp->x1 && end <= cp->x2)))
            break;

    if (cp) {
        cp->y = y;
        cp->a += end - beg;
        if (end - beg > cp->d)
            cp->d = end - beg;
    } else {
        if (!(cp = malloc(sizeof(*cp))))
            exit(1);
        *cp = (Circle){ .next = *list, .x1 = beg, .x2 = end, .y = y,
                        .d = end - beg, .a = end - beg };
        *list = cp;
    }
}

int main(void)
{
    int w, h, x, y, p, in, beg;
    Circle *cp, *list = NULL;

    scanf("P3 %d %d %*d", &w, &h);

    for (y = 0; y < h; y++) {
        in = 0;

        for (x = 0; x < w; x++) {
            scanf("%d %*d %*d", &p);

            if (!p && !in) {
                beg = x;
                in  = 1;
            } else if (p && in) {
                in  = 0;
                update(&list, beg, x, y);
            }
        }
        if (in)
            update(&list, beg, w, y);
    }
    for (cp = list; cp; cp = cp->next)
        printf("%f\n", cp->a / ((cp->d / 2.) * (cp->d / 2.)));

    return 0;
}

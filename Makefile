.POSIX:

# rely on POSIX default rules for .c and .sh, add rule for .awk
# this assumes .sh has the #!/bin/sh line which is acceptable as sh is
# required to be installed in /bin/. awk is not required to be installed
# in any given location so use command to find it and add the shebang

.SUFFIXES: .awk

.awk:
	type $(AWK) > /dev/null # make sure we have awk
	{ printf '#!%s -f\n' "$$(command -v $(AWK))"; cat $<; } > $@
	chmod a+x $@

# my system has awk -> gawk but I prefer using nawk, still only use
# POSIX features except where noted
AWK = nawk

# POSIX specifies CC = c99 but I enjoy using C11 and given CFLAGS
CC     = gcc
CFLAGS = -std=c11 -pedantic -Wall -Wextra -O3
